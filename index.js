// Use the "require" directive to load the express module/package
const express = require("express");

// Create an application using express
const app = express();

// Port to listen to
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows your app to read json data
app.use(express.json());

// Allows the app to read data from forms
// Applying the option "extended:true" allows us to recieve information in other data types such as an object
app.use(express.urlencoded({extended:true}));

// Create a GET route
app.get("/", (req, res) => {
	res.send("Hello, world!");
})

//ANother GET route with "/hello" endpoint
app.get("/hello",(req,res)=>{res.send("Hello from the /hello endpoint!")});

//Create a POST route
app.post("/hello",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
});

/*
POSTMAN
{
    "firstName":"John",
    "lastName":"Doe"
}
*/


//Create a POST route to register a user
let users = [];
app.post("/signup",(req,res)=>{
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else{
		res.send("Please input BOTH username and password.");
	}
});
/*
POSTMAN:
{
    "username":"johndoe",
    "password":"12356546"
}
*/

//CReate a PUT route ro change the password of a specific user.
app.put("/change-password",(req, res)=>{
	//Create a variable to store the message to be sent back to the client
	let message;

	//Create a for loop that will loop through the elements of the "users" array
	for(let i=0; i< users.length;i++){
		//req.body == juan23 -- user[i].username == juan23
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			// CHange the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated`;
			break;
		} else{
			message = `User does not exist`;
		}
	}
	res.send(message);
})

/*
POSTMAN:
{
    "username":"johndoe",
    "password":"12356546"
}
*/


//=======================> ACTIVITY HERE

//1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home",(req,res)=>{res.send("Welcome to the home page")});


//2. Process a GET request at the "/home" route using postman.
//OK

//3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
let x = users;
app.get("/users",(req,res)=>{
		res.send(x);
});

//4. Process a GET request at the "/users" route using postman.==OK	

//5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user",(req,res)=>{
	let msg;
	for(let i=0; i< users.length;i++){
		if(req.body.username === users[i].username){
			users.splice(i, 1);
			msg = `User  ${req.body.username} has been deleted.`
			break;
		} else{
			msg = `User does not exist`;
		}
	}
	res.send(msg);
});

//6. Process a DELETE request at the "/delete-user" route using postman.






// Tells our server to listend to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
